package br.com.finalelite.report.commands;

import br.com.finalelite.pauloo27.api.commands.BaseCommand;
import br.com.finalelite.pauloo27.api.commands.usage.DefaultParameterType;
import br.com.finalelite.pauloo27.api.commands.usage.Parameter;
import br.com.finalelite.pauloo27.api.commands.usage.ParameterType;
import br.com.finalelite.report.Main;
import lombok.val;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;

public class ReportCommand extends BaseCommand {

    public ReportCommand() {
        super("report");
        setAliases(Arrays.asList("reporte", "reportar"));
        usage.addParameter(new Parameter("discord ou livro", DefaultParameterType.STRING, true));
        usage.setUsageMessageFormat("&aSe quiser reportar um jogador, use &f/denunciar&a. " +
                "Para reportar bugs você pode usar &f/reportar livro&a ou &f/reportar discord&a.");

        playerListener = cmd -> {
            val player = cmd.getSender();

            val type = (String) cmd.getArgument(0);

            if (type.equalsIgnoreCase("discord")) {
                cmd.reply("&aEntre no Discord e use &f!suporte <assunto>&a no canal &f#suporte&a.\nEntre no nosso Discord: &fhttps://discord.gg/RzWnB26&a.");
                return true;
            } else if (type.equalsIgnoreCase("livro")) {
                if (Main.getInstance().getReportManager().isWaiting(player)) {
                    cmd.reply("&cVocê já está reportando um bug. Será que perdeu seu livro?");
                    return true;
                }

                if (!Main.getInstance().getDatabase().canCreate(player.getUniqueId().toString())) {
                    cmd.reply("&cVocê fez spam do comando e por isso não pode usar novamente.");
                    return true;
                }

                cmd.reply(ChatColor.GREEN + "Escreva seu problema nesse livro e assine. Para cancalear, drope o livro. Spam desse sistema pode gerar ban.");
                val book = new ItemStack(Material.WRITABLE_BOOK);
                val bookMeta = book.getItemMeta();
                bookMeta.setDisplayName(ChatColor.GREEN + "Reportar");
                bookMeta.setLore(Arrays.asList(ChatColor.AQUA + "Escreva seu problema no livro", ChatColor.AQUA + "Assine para enviar o reporte", ChatColor.AQUA + "Drope o livro para cancelar"));
                book.setItemMeta(bookMeta);

                player.getInventory().addItem(book);
                Main.getInstance().getReportManager().toggleWaiting(player);

                return true;
            }
            return false;
        };
    }

}
