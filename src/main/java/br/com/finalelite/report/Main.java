package br.com.finalelite.report;

import br.com.finalelite.pauloo27.api.PauloAPI;
import br.com.finalelite.pauloo27.api.commands.CommandablePlugin;
import br.com.finalelite.report.commands.ReportCommand;
import br.com.finalelite.report.database.Database;
import br.com.finalelite.report.listeners.PlayerListener;
import br.com.finalelite.report.utils.ReportManager;
import lombok.Getter;
import lombok.val;

public class Main extends CommandablePlugin {

    @Getter
    private ReportManager reportManager;
    @Getter
    private static Main instance;
    @Getter
    private Database database;

    public void onEnable() {
        instance = this;
        reportManager = new ReportManager();

        val db = PauloAPI.getInstance().getDatabaseInformation();
        val address = db.getAddress();
        val port = db.getPort();
        val username = db.getUsername();
        val password = db.getPassword();

        database = new Database(address, port, username, password);
        database.connect();

        getServer().getPluginManager().registerEvents(new PlayerListener(), this);
        registerCommand(new ReportCommand());
    }

}
