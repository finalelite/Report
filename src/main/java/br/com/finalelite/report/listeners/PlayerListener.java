package br.com.finalelite.report.listeners;

import br.com.finalelite.report.Main;
import lombok.val;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerEditBookEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.scheduler.BukkitRunnable;

public class PlayerListener implements Listener {

    @EventHandler
    public void onBookClose(PlayerEditBookEvent event) {
        val player = event.getPlayer();
        val oldMeta = event.getPreviousBookMeta();
        val meta = event.getNewBookMeta();

        val isAnReportBook = oldMeta.getDisplayName().equals(ChatColor.GREEN + "Reportar");

        if (!event.isSigning()) {
            if (Main.getInstance().getReportManager().isWaiting(player)) {
                player.sendMessage(ChatColor.YELLOW + "Assine o livro para enviar o reporte.");
            }
            return;
        }

        if (!Main.getInstance().getReportManager().isWaiting(player)) {
            if (isAnReportBook) {
                player.sendMessage(ChatColor.RED + "Você não está reportando algo.");
                removeLater(player, oldMeta);

                event.setCancelled(true);
            }
            return;
        }

        if (!isAnReportBook)
            return;

        val message = String.join("\n", meta.getPages()).replace(ChatColor.BLACK.toString(), "");
        val report = Main.getInstance().getReportManager().createReport(player, message);
        Main.getInstance().getReportManager().toggleWaiting(player);
        player.sendMessage(ChatColor.GREEN + "Seu reporte foi enviado, obrigado. ID: " + report.getId() + ".");
        removeLater(player, oldMeta);
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onItemDrop(PlayerDropItemEvent event) {
        if (event.isCancelled())
            return;

        val player = event.getPlayer();
        val item = event.getItemDrop().getItemStack();
        val meta = item.getItemMeta();

        if (item.getType() != Material.WRITABLE_BOOK)
            return;

        val isAnReportBook = meta.getDisplayName().equals(ChatColor.GREEN + "Reportar");
        if (!isAnReportBook)
            return;

        event.getItemDrop().remove();

        if (Main.getInstance().getReportManager().isWaiting(player)) {
            player.sendMessage(ChatColor.RED + "Seu reporte foi cancelado.");
            Main.getInstance().getReportManager().toggleWaiting(player);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onInventoryClick(InventoryClickEvent event) {
        val player = (Player) event.getWhoClicked();

        if (Main.getInstance().getReportManager().isWaiting(player)) {
            player.sendMessage(ChatColor.RED + "Por favor, reporte o bug ou drope o livro para cancelar.");
            event.setCancelled(true);
        }
    }

    private void removeLater(Player player, BookMeta meta) {
        new BukkitRunnable() {
            @Override
            public void run() {
                player.getInventory().all(Material.WRITABLE_BOOK).forEach((slot, o) -> {
                    if (o.getItemMeta().equals(meta))
                        player.getInventory().setItem(slot, new ItemStack(Material.AIR));
                });
            }
        }.runTaskLater(Main.getInstance(), 5);
    }


}
