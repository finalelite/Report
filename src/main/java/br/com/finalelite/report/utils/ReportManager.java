package br.com.finalelite.report.utils;

import br.com.finalelite.report.Main;
import br.com.finalelite.report.database.Report;
import lombok.val;
import lombok.var;
import org.bukkit.entity.Player;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ReportManager {

    private List<Player> waitingForCloseTheBook = new ArrayList<>();

    public boolean isWaiting(Player player) {
        return waitingForCloseTheBook.contains(player);
    }

    public void toggleWaiting(Player player) {
        if (isWaiting(player))
            waitingForCloseTheBook.remove(player);
        else
            waitingForCloseTheBook.add(player);
    }

    public Report createReport(Player player, String message) {
        if (message.length() >= 1000)
            return null;

        val createAt = new Date().getTime() / 1000;
        val author = player.getUniqueId().toString();
        var id = -1;

        try {
            id = Main.getInstance().getDatabase().insertReport(author, message, createAt);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return new Report(id, author, message, createAt, false);
    }

}
