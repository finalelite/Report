package br.com.finalelite.report.database;

import com.gitlab.pauloo27.core.sql.*;
import lombok.RequiredArgsConstructor;
import lombok.val;

import java.sql.SQLException;

@RequiredArgsConstructor
public class Database {

    private EzSQL sql;

    private final String address;
    private final int port;
    private final String username, password;
    private EzTable reports;

    public void connect() {
        try {
            sql = new EzSQL(EzSQLType.MYSQL)
                    .withDefaultDatabase("mixedup")
                    .withAddress(address, port)
                    .withLogin(username, password)
                    .withCustomDriver("com.mysql.jdbc.Driver")
                    .connect();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            reports = sql.createIfNotExists(new EzTableBuilder("reports")
                    .withColumn(new EzColumnBuilder("id", EzDataType.PRIMARY_KEY))
                    .withColumn(new EzColumnBuilder("author", EzDataType.VARCHAR, 36))
                    .withColumn(new EzColumnBuilder("message", EzDataType.VARCHAR, 1000, EzAttribute.NOT_NULL))
                    .withColumn(new EzColumnBuilder("createAt", EzDataType.BIGINT, EzAttribute.NOT_NULL))
                    .withColumn(new EzColumnBuilder("spam", EzDataType.BOOLEAN).withDefaultValue(false)));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int insertReport(String author, String message, long createAt) throws SQLException {
        reports.insertAndClose(new EzInsert("author, message, createAt", author, message, createAt));

        try (val result = reports.select(new EzSelect("id")
                .where().equals("author", author).and()
                .equals("createAt", createAt).and()
                .equals("message", message).limit(1)).getResultSet()) {

            if (result.next())
                return result.getInt("id");
        }
        return -1;
    }

    public boolean canCreate(String player) {
        try (val result = reports.select(new EzSelect("id")
                .where().equals("author", player)
                .and().equals("spam", true).limit(1)).getResultSet()) {
            return !result.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

}
