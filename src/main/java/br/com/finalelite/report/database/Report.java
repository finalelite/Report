package br.com.finalelite.report.database;

import lombok.Data;

@Data
public class Report {
    private final int id;
    private final String author;
    private final String message;
    private final long createAt;
    private final boolean isSpam;
}
